---
title: Countdown Timer with LiveData
date: 2020-04-20 19:46:42
tags: Android, Android Development, LiveData, Kotlin
---

![Featured Image](/2020/04/20/Countdown-Timer-with-LiveData/post-header.jpg)

Photo by [Martin Adams](https://unsplash.com/@martinadams) on [Unsplash](https://unsplash.com)

# Backstory

** TL;DR [Implementation](#Implementation) **

I was trying to achieve a countdown timer for one of my app [Warframe Companion](https://gitlab.com/technowolf/warframe-companion). I did create simple countdown timer just like most people by extending `CountDownTimer` class and customizing it according to my needs.

But I had to create extensions in my Base classes to remove references of countdown timers to prevent any possible memory leaks and null pointer exceptions when timer ends in background. But since methods were abstract and I had to implement them in every UI class, I gave up on it and tried to bind CountDownTimer to lifecycle of views. However, that added more boilerplate code and made everything spaghetti.

When I was about to give up on the idea to bind everything to lifecycle being in deep sadness about my spaghetti code, I had a weird and vague idea about using LiveData to achieve CountDownTimer functionality. But how?

# Implementation

## **Create a class that extends LiveData.**

In my particular use case, I decided to return string from my custom class,

```kotlin
class TimerLiveData : LiveData<String>()
```

## **Create Handler instance**

Handler will help us to send and receive message to our Runnable process. 

Create runnable but don't try to initialize it, Because you simply can't, DUH!

```kotlin
    private val handler: Handler = Handler()
    private val runnable: Runnable
```

## **Override onActive() and onInactive() methods**

When you override onActive() call post() on handler and pass the runnable we just created.

```kotlin
override fun onActive() {
    super.onActive()
    handler.post(runnable)
}
```

Also, Don't forget to remove callbacks from handler with removeCallbacks()

```kotlin
override fun onInactive() {
    super.onInactive()
    handler.removeCallbacks(runnable)
}
```

## **Expose one property to be initialized from outside**

In my use case I have exposed one Long type variable totalSeconds to set total number of seconds into it.

```Kotlin
var totalSeconds: Long = 0
```

## **Now the fun part, Write the logic in init{}**

It's time to override run() method and perform what we want to on specific interval.

I want callback to my LiveData every second so I wil implement my run() method like this,

```kotlin
init {
    runnable = object : Runnable {
        override fun run() {
            // Pre decrement total seconds 
            --totalSeconds
            // Send value back to the LiveData observer
            this.value = totalSeconds.toString()
            // Set delay of 1 second.
            handler.postDelayed(this, 1000)
        }
    }
}
```

And that's it. You are done. 

Depending on your implementation, Sometimes you can't access value property with this keyword. Just try this@YourCustomClass.value and you can access it. 

But wait, How to listen it in my UI? 

## **Implementing it in your UI**

First things first, Declare and initialize your custom class.

```kotlin
private val myAwesomeTimer = AwesomeTimer()
```

Then simply observe your LiveData

```kotlin
// Set the property which we have exposed to set from outside
 myAwesomeTimer.totalSeconds = 60L
        myAwesomeTimer.observe(viewLifecycleOwner, Observer {
            // Since my LiveData was created to return string, it will be of string type. 
            Log.d("TAG",it)
            // Check your logcat, you will see callbacks at every one second.
        })
```

Yes, It was that simple. Since LiveData attaches itself to lifecycle of the view it is being observed, It will be paused, cleared, and destroyed for us automatically with the view.

I know this approach can be improved by setting the totalSeconds value through constructor or by extension that does that for you, but for sake of this article I decided to keep it as simple as possible.

That's it for this one, I will post some more findings when I encounter them.

Thanks for stopping by, Have a good day :)